\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{practical}[2021/06/05 Practical Typography paper template]

\newcommand\txtwidth{2.3}
\newcommand\ptsize{12pt}

\DeclareOption{10pt}{
  \renewcommand\ptsize{10pt}
}
\DeclareOption{11pt}{
  \renewcommand\ptsize{11pt}
}

\DeclareOption{wide}{
  \renewcommand\txtwidth{2.6}
}

\DeclareOption{numcite}{
  \renewcommand*{\citesetup}{%
    \lining
    \biburlsetup
    \frenchspacing}
}

%% \DeclareOption{width}{
%%   %% TODO
%% }

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}
\ProcessOptions\relax

\PassOptionsToClass{\ptsize}{article}
\LoadClass{article}

%% Basic packages
\RequirePackage{graphicx}
\RequirePackage{booktabs}
\RequirePackage{subcaption}

%% Text area
\newlength{\alphabet}
\settowidth{\alphabet}{\normalfont abcdefghijklmnopqrstuvwxyz}
\PassOptionsToPackage{textwidth=\txtwidth\alphabet}{geometry}
\RequirePackage{geometry}

\RequirePackage{fontspec}
\RequirePackage{unicode-math}
\setmainfont[
  Ligatures=TeX,
  Numbers={OldStyle, Proportional},
  SmallCapsFeatures={LetterSpace=3, Renderer=Basic}
                       % Ligatures={NoCommon, NoRequired, NoContextual}}
]{Linux Libertine O}
\setmathfont[Scale=MatchUppercase]{libertinusmath-regular.otf}
\setmonofont[Scale=MatchLowercase]{DejaVu Sans Mono}
\RequirePackage{realscripts}
\RequirePackage{etoolbox}
\renewcommand{\arraystretch}{1.2}
\frenchspacing

%% Equations
\newcommand\lining{\addfontfeatures{Numbers={Monospaced, Lining}}}
\AtBeginEnvironment{tabular}{\lining}
\renewcommand{\theequation}{{\lining\arabic{equation}}}

%% Section style
\RequirePackage{titlesec}
\newcommand{\flatcaps}[1]{\textsc{\MakeLowercase{#1}}}
\titleformat{\section}{\large\bfseries}{\lining\thesection}{1em}{}
\titleformat{\subsection}{\large}{\thesubsection}{.6em}{\flatcaps}
\titleformat{\subsubsection}{}{\thesubsubsection}{.6em}{\itshape}
\titleformat{\paragraph}[runin]{\scshape}{\theparagraph}{0pt}{}[.]
\titlespacing*{\section}{0pt}{2\baselineskip}{1\baselineskip}
\titlespacing*{\subsection}{0pt}{1\baselineskip}{1\baselineskip}
\titlespacing*{\subsubsection}{0pt}{8pt}{5pt}

%% Microtype
\PassOptionsToPackage{protrusion}{microtype}
\PassOptionsToPackage{expansion}{microtype}
\RequirePackage{microtype}

%% Lists
\RequirePackage{enumitem}
\setlist{noitemsep, nosep, leftmargin=\parindent}
\setlist[1]{labelindent=\parindent} % < Usually a good idea
\setlist[description]{font=\mdseries\flatcaps, labelindent=0pt} % Have to remove the bold

%% Captions
\RequirePackage{caption}
\DeclareCaptionLabelFormat{lc}{\flatcaps{#1}~{#2}}
\captionsetup{font=small, format=plain, labelformat=lc}
\captionsetup[subfigure]{labelsep=colon, labelfont=sc, labelformat=simple}

%% Footnotes
\PassOptionsToPackage{norule, ragged, flushmargin}{footmisc}
\RequirePackage{fnpct}
\RequirePackage{footmisc}
\renewcommand\footnotemargin{\parindent}
\makeatletter
\renewcommand\@makefntext[1]{%
    \@thefnmark.~#1}
\makeatother

%% Abstract
\PassOptionsToPackage{runin}{abstract}
\RequirePackage{abstract}
\renewcommand{\abstractnamefont}{\normalfont\flatcaps}
\abslabeldelim{\newline}
\setlength{\abstitleskip}{-\parindent}
